HOW TO RUN THE PROJECT : 

1) In file C:\xampp\apache\conf\extra\httpd-vhosts.conf
create a virtual host , with Document Root : project path 
                        and server name
						
<VirtualHost *:8080>
	DocumentRoot "C:/xampp/htdocs/phplib"
	ServerName www.phponlinelibrary.com
</VirtualHost>


2)In file C:\Windows\System32\drivers\etc\hosts
add to "localhost name resolution is handled within DNS itself." : 
	
	127.0.0.1       phponlinelibrary.com

3)In phpmyadmin create database with the name : onlinelibrary2 
  and upload the database that is in  phplib\database
  
  if you want to change the database name for the hole project 
  go to phplib\engine\Config.php where you can find all the info you need