-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 
-- Версия на сървъра: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `onlinelibrary2`
--

-- --------------------------------------------------------

--
-- Структура на таблица `books`
--

CREATE TABLE IF NOT EXISTS `books` (
`id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `author` varchar(50) NOT NULL DEFAULT 'undefined',
  `price` decimal(10,0) NOT NULL DEFAULT '0',
  `description` text,
  `picture` varchar(500) NOT NULL DEFAULT 'none',
  `category` varchar(500) NOT NULL DEFAULT 'none'
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Схема на данните от таблица `books`
--

INSERT INTO `books` (`id`, `title`, `author`, `price`, `description`, `picture`, `category`) VALUES
(1, 'Pretend With Me', 'Jemma Grey', '20', 'Three weeks after 17 year old Jen witnesses the murder of her boyfriend, Daren she flees her Trinidadian home, in hopes of forgetting, and for a time she does. That changes when she meets the Wilsons, and learns that her wedding to Eric Wilson had been pre-arranged since her birth. \r\nIn an instant everything she knows about herself is lost when she discovers the truth about the night that Daren died. The Wilsons aren’t who she thinks; they belong to an untainted breed of vampires that can only be born - the purebloods. \r\nJen finds herself in a world suddenly filled with supernatural creatures and in the middle of a war between an army fighting to protect her and a pack of shifters trying to kill her and in the midst of it all she’s struggling to find herself as her wedding looms closer, signifying a union other than marriage.', 'coverpic3d.png', 'Fantasy'),
(2, 'Haunted', 'Sammantha Lewis', '30', 'An alpha male, a girls who''s supernatural powers are on overload,a ghost that like to meddle, and a secret village of supernaturals. Yup that''s a good mix. \r\n\r\nAvery didn''t live a happy life like she wanted. No instead she lived horrible life with her foster parents at home and a life as a wall flower at school. And being able to see dead people did not help her self esteem. Needless to say Jericho, Levi never even spared her a glance until now. To Avery, Jericho is just toying with her only to leave her more alone then before. But to Jericho Avery is his mate and he has no intention of letting her go. However that means he has to save her, and keep her from leaving.', 'coverpic4.png', 'Fantasy'),
(3, 'New Life', 'H.N. S', '40', 'Mia Owens and her mother have just moved to California from England to find a better life. \r\n\r\nMia just wants to live easy, go to school, hag out with friends... that was until Ian Marsh turned her life upside down. \r\n\r\nIan Marsh is rich and popular and can get whatever he wants, but there''s one thing that he wants more than anything, one thing his money and charm can''t get... Mia! \r\nWill he work hard for once to get her and keep her? \r\nWill Mia let Ian help her? ', 'coverpic5.png', 'Romance'),
(4, 'Destined Love', 'Marline', '10', 'Princess Cleopatra has to work together with the arrogant but extremely handsome Prince Durwald. Will she be able to complete her job successfully without Prince Durwald stealing her heart? Or will she fall for his charms?', 'coverpic6.png', 'Romance'),
(5, 'Bloody Mary', 'Jessica Pham', '50', 'Kimberly, also known as, Kim, was just playing a harmless game of ''Bloody Mary'', right? Nah. It isn''t harmless at all. She had to find that out the hard way.', 'coverpic7.png', 'Thriller'),
(6, 'JACK THE RIPPER 6 Word Challenge', 'Cleveland W. Gibson', '60', 'In Victorian times ''Jack'' was feared by every prostitute in London, and hunted by the Police and the best brains in England. How was it that in a pea-souper(thick fog) he still managed to carry out his butchery when others could hardly see in those awful conditions in the dimly lit street of London?', 'coverpic8.png', 'Thriller'),
(7, 'Forbidden Fantasy', 'Nina Kari', '35', 'It all began with a stolen kiss.\r\nLayla Bungah had a normal life before the school system threw a new teacher into the mix. She had a boyfriend that loved her, a best friend that was more of a sister than anything, and a father that trusted her, even though she kept secrets from him daily. Devin Simmons has decided to give being an English teacher a chance since that was his actual major in college. He just wanted to have the thrill of teaching young people what he knew and what he was taught. He never expected to be teaching one of his students other things. When Layla comes to class one day she’s disappointed that one of her favorite teacher was fired and replaced so easily. She soon finds out the new teacher isn’t so bad, not to mention hunkalisious. But Layla isn''t the only one guilty of fantasizing about her sexy new teacher. Devin often stares at Layla with affection, and Layla can’t help but think he feels the same way. Soon Devin and Layla have a secret. A secret that really shouldn’t get out or it could cause trouble for the both of them.', 'coverpic9.png', 'Fiction'),
(8, 'Rain on My Wings', 'Juliet Rose', '25', '"I wanted him to see me, to open his eyes and simply acknowledge my presence. But I knew he wouldn’t. To him, I was only a speck in the universe, a beautiful but unimportant creature. He would not see me, because right now... I was a butterfly."', 'coverpic10.png', 'Fiction'),
(9, 'Salem', 'Stephen King', '55', 'A great story by Stephen King and his wild imagination. This story is one of the best written by Stephen King, and perhaps one of the most memorable.', 'coverpic11.png', 'Horror'),
(10, 'We All Fall Down', 'Dale A. Moses', '48', 'Burning to death is a horrible fate. Being forced to watch it happen night after night to innocent children is even worse. What do you do when there is nothing that you can do? What would you do when its already too late......or is it?', 'coverpic12.png', 'Horror'),
(11, 'My P.I.M.P ', 'Crysah Cue', '33', '"She opened the door,closed her eyes and took a deep breath in. She opened her eyes and almost fainted trying to let the air out. The boy from earlier sat on her bed going through her underwear drawer like he was on the beach searching for shells to make a castle."\r\n\r\n\r\nHaving her own apartment, is like the best thing ever for Mona. Until a boy rises out of her floor, and her beautiful high school Life she was looking forward to takes a drastic turn.', 'coverpic13.png', 'Erotic'),
(12, 'The Contract', 'Mia Elysse', '61', 'This is just the first of many. My stories consist of short stories about random shit. Some funny, some sexual or even dangerous. My goal is to capture your attnetion. If I do so then my job is half way complete.', '9bf5858e3a6a92958434562b04ef8b0c.png', 'Erotic');

-- --------------------------------------------------------

--
-- Структура на таблица `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `egn` char(10) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `fnamelname` varchar(100) NOT NULL,
  `phonenumber` char(15) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `isadmin` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Схема на данните от таблица `users`
--

INSERT INTO `users` (`id`, `egn`, `username`, `password`, `fnamelname`, `phonenumber`, `email`, `isadmin`) VALUES
(1, '9202204556', 'admin', 'adminpass', 'Admin Adminov', '089845654312312', 'admin@gmail.com', 1),
(7, '4189489468', 'user', 'userpass', 'User Userov', '054897494564', 'user@mail.com', 0),
(8, '9407074544', 'zlatko0o', 'zlatkopass', 'Zlatko P.', '008486435459', 'zlatko@gmail.com', 1),
(9, '9402205656', 'ani', 'anipass', 'Anna Krivova', '0893737120', 'ani94@mail.bg', 0),
(10, '9805068856', 'someuser2', 'someuser2pass', 'Some User 2', '0892656524', 'someuser2@gmail.com', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
