<?php

class Books extends Model {

    function GetAllBooks(){
        $result = array();
        $cmd = $this->mysqli->stmt_init();
        $query =  '
        SELECT *
        FROM `books`
        ORDER BY `id` ASC
        ';

        if ($cmd->prepare($query))
        {
            $cmd->execute();
            $reader = $cmd->get_result();
            while ($row = $reader->fetch_assoc())
            {
                $result[$row['category']][] = $row;
            }
            $reader->close();
            $cmd->close();
        }
        return $result;
    }

    function AddBook($book){
        $result = array();
        $cmd = $this->mysqli->stmt_init();
        $query ='
        INSERT INTO `books`
        (`title`, `author`, `price`, `description`, `picture`, `category`)
        VALUES (?,?,?,?,?,?)
        ';

        if ($cmd->prepare($query))
        {
            $cmd->bind_param('ssdsss',$book['title'], $book['author'], $book['price'], $book['description'], $book['picture'], $book['category']);
            $cmd->execute();
            $cmd->close();
        }
    }

    function UpdateBook($book){
        $result = array();
        $cmd = $this->mysqli->stmt_init();
        $query ='
        UPDATE `books` SET
        `title`=?,
        `author`=?,
        `price`=?,
        `description`=?,
        `picture`=?,
		`category` = ?
        WHERE
        `id`=?
        ';
        if ($cmd->prepare($query))
        {
            $cmd->bind_param('ssdsssi',$book['title'], $book['author'], $book['price'], $book['description'], $book['picture'],$book['category'],$book['id']);
            $cmd->execute();
            $cmd->close();
        }
    }

    function DeleteBook($id){
            $result = array();
        $cmd = $this->mysqli->stmt_init();
        $query = '
            DELETE FROM `books`
            WHERE `id`=?
            ';
            if ($cmd->prepare($query))
            {
                $cmd->bind_param('i', $id);
                $cmd->execute();
            }

        }
 
	public function getBookBy(array $args = array())
	{
		$result = false;
        $cmd = $this->mysqli->stmt_init();
        $query = '
        SELECT *
        FROM `books`
        WHERE 1=1
        ';
		
		$keys = array_keys($args);
		
		foreach($keys as $k){
			$query .= "AND `{$k}`=?";
		}

		
        if ($cmd->prepare($query))
        {
			foreach($args as $value)
			{
				$cmd->bind_param('s', $value);
			}
            
            $cmd->execute();
			$res = $cmd->get_result();
			
			return $res->fetch_assoc();
        }
		
        return false;
	}
	
	function GetBookByCategory($category){
		$result = array();
			$cmd = $this->mysqli->stmt_init();
			$query ='
            SELECT `id`, `title`, `author`, `price`, `description`, `picture`
            FROM `books`
            WHERE `category`=?
            ';

            if ($cmd->prepare($query))
            {
                $cmd->bind_param('s', $category);
                $cmd->execute();
                $reader = $cmd->get_result();
                if ($row = $reader->fetch_assoc())
                {
                    $result = $row;
                }

                $reader->close();
                $cmd->close();
            };
        return $result;
	}
	
}
/*
function GetBookById($id){
            $result = array();
			$cmd = $this->mysqli->stmt_init();
			$query ='
            SELECT `id`, `title`, `author`, `price`, `description`, `picture`
            FROM `books`
            WHERE `id`=?
            ';

            if ($cmd->prepare($query))
            {
                $cmd->bind_param('i', $id);
                $cmd->execute();
                $reader = $cmd->get_result();
                if ($row = $reader->fetch_assoc())
                {
                    $result = $row;
                }

                $reader->close();
                $cmd->close();
            };
            return $result;
    }
*/
 ?>
