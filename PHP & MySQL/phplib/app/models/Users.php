<?php


class Users extends Model {
	
    function GetAllUsers(){
        $result = array();
        $cmd = $this->mysqli->stmt_init();
        $query = '
        SELECT *
        FROM `users`
        ORDER BY `fnamelname` ASC
        ';

        if ($cmd->prepare($query))
        {
            $cmd->execute();
            $reader = $cmd->get_result();
            while ($row = $reader->fetch_assoc())
            {
                $result[] = $row;
            }
            $reader->close();
            $cmd->close();
        }
        return $result;
    }

    function AddUser($user){
        $result = null;
        $cmd = $this->mysqli->stmt_init();
        $query ='
        INSERT INTO `users`
        (`egn`, `username`, `password`, `fnamelname`, `phonenumber`, `email`, `isadmin`)
        VALUES (?,?,?,?,?,?,?)
        ';

        if ($cmd->prepare($query))
        {
            $cmd->bind_param('ssssssi',$user['egn'], $user['username'], $user['password'], $user['fnamelname'], $user['phonenumber'], $user['email'], $user['isadmin']);
            $cmd->execute();
            $cmd->close();
        }
    }

    function UpdateUser($user){
        $result = null;
        $cmd = $this->mysqli->stmt_init();
        $query ='
        UPDATE `users` SET
        `egn`=?,`username`=?,
        `password`=?,
        `fnamelname`=?,
        `phonenumber`=?,
        `email`=?,
        `isadmin`=?
		WHERE id = ?
        ';
        if ($cmd->prepare($query))
        {
            $cmd->bind_param('ssssssii',$user['egn'], $user['username'], $user['password'], $user['fnamelname'], $user['phonenumber'], $user['email'],$user['isadmin'], $user['id']);
            $cmd->execute();
        }
    }

    function DeleteUser($id){
        $cmd = $this->mysqli->stmt_init();
        $query = '
        DELETE FROM `users`
        WHERE `id`=?
        ';
        if ($cmd->prepare($query))
        {
            $cmd->bind_param('i', $id);
            $cmd->execute();
        }
    }

    public function AuthenticateUser($username, $password)
    {
        $result = false;
        $cmd = $this->mysqli->stmt_init();
        $query = '
        SELECT *
        FROM `users`
        WHERE `username`= ? AND `password`= ?
        ';

        if ($cmd->prepare($query))
        {
            $cmd->bind_param('ss', $username, $password);
            $cmd->execute();

            $result = $cmd->get_result();
			$rows = $result->num_rows;
            if($rows > 0)
            {
                return true;
            }
        }
		
        return false;
    }
	
	public function getUserByUsername($username) {
		$result = false;
        $cmd = $this->mysqli->stmt_init();
        $query = '
        SELECT *
        FROM `users`
        WHERE `username`= ?
        ';

        if ($cmd->prepare($query))
        {
            $cmd->bind_param('s', $username);
            $cmd->execute();
			$res = $cmd->get_result();
			
			return $res->fetch_assoc();
        }
		
        return false;
	}
	
	public function getUserBy(array $args = array())
	{
		$result = false;
        $cmd = $this->mysqli->stmt_init();
        $query = '
        SELECT *
        FROM `users`
        WHERE 1=1
        ';
		
		$keys = array_keys($args);
		
		foreach($keys as $k){
			$query .= "AND `{$k}`=?";
		}

		
        if ($cmd->prepare($query))
        {
			foreach($args as $value)
			{
				$cmd->bind_param('s', $value);
			}
            
            $cmd->execute();
			$res = $cmd->get_result();
			
			return $res->fetch_assoc();
        }
		
        return false;
	}
}

?>
