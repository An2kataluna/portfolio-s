<?php

class Model {
	
	protected $config;
	protected $mysqli;
	
	public function __construct($config)
	{
		$this->mysqli = new mysqli($config['mysql']['host'], $config['mysql']['user'], $config['mysql']['password'], $config['mysql']['db']);
		$this->mysqli->set_charset('utf8');
	}
}
 ?>
