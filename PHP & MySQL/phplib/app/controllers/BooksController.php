<?php


class BooksController extends BaseController {

	public function addBook()
    {
		$errors=array();
		
		
		
		
		if(isset($_POST['addbook'])){
			if(!isset($_POST['title'])){
				$errors[] = 'Please Enter Title of the book!';
			}
			
			if(isset($_FILES['cover']))
			{
				$newName = $this->config['engine']['basePath']."content".DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR;
				$curImgExt = pathinfo($_FILES['cover']['name'], PATHINFO_EXTENSION);
				$imgName = md5(uniqid()).".".$curImgExt;
				$builtName = $newName.$imgName;
				move_uploaded_file($_FILES['cover']['tmp_name'], $builtName);
				$_POST['picture'] = $imgName;
				
			}
			
			if(count($errors) === 0){
				$this->loadModel('Books')->AddBook($_POST);
			}
			
		}
		
        $this->loadView('AddBookForm');
    }

    public function updateBook($params)
    {
		$id = array_pop($params);
		if((int)$id <= 0)
		{
			$this->redirect('/');
			return;
		}
		
		$book = $this->loadModel('Books')->getBookBy(array(
			'id' => $id
		));
		

		if(!$book)
		{
			$this->redirect('/');
			return;
		}
		
		if(isset($_POST['updatebook']))
		{
			$_POST['id'] = $id;
			if(empty($_FILES))
			{
				$_POST['picture'] = $book['picture'];
			}
			$this->loadModel('Books')->updateBook($_POST);
			
		} else {
			$_POST = $book;
		}
		
        $this->loadView('UpdateBookForm');
    }
	public function deleteBook($params)
    {
		
		$id = array_pop($params);
		
		if(!is_numeric($id))
		{
			$this->redirect('/');
			return;
		}
		
		$this->loadModel('Books')->deleteBook($id);
		
		$this->redirect('/books/list');
	}
	public function getAll(){
		$result=$this->loadModel('Books')->GetAllBooks();
		$this->loadView('BooksList',  ['books' => $result]);
	}
}
