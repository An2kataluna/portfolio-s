<?php
    session_start(); // Starting Session
    $error=''; // Variable To Store Error Message
    if (isset($_POST['login'])) {
        if (empty($_POST['username']) || empty($_POST['password'])) {
			$error = "Username or Password is empty";
		}
	}
    else
        {
        // Define $username and $password
        $username=$_POST['username'];
        $password=$_POST['password'];

        $user=new Users();
        $validation=$user->AuthenticateUser($username,$password);
        if ($validation) {
            header("Location: AdminViewList.php");
        }
        else{
            echo "Invalid User";
        }
    }
 ?>
