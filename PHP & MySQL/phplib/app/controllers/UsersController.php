<?php


class UsersController extends BaseController
{
	public function addUser()
	{

		if(!isset($_SESSION['logged']) && $_SESSION['loggedUser']['isadmin']!=1){
			$this->redirect('/');
			return;
		}

		$errors=array();

		if(isset($_POST['adduser'])){

			$user = $this->loadModel('Users')->getUserBy(array(
				'email' => $_POST['email']
			));

			if($user)
			{
				$errors[] = "Choose another email";
			}

			$user = $this->loadModel('Users')->getUserBy(array(
				'username' => $_POST['username']
			));
			if($user)
			{
				$errors[] = "Choose another username";
			}

			$user = $this->loadModel('Users')->getUserBy(array(
				'egn' => $_POST['egn']
			));
			if($user)
			{
				$errors[] = "Egn allready exists";
			}

			if(count($errors) === 0){
				$_POST['isadmin'] = 1;
				$this->loadModel('Users')->addUser($_POST);
			}
		}

		$this->loadView('AddUserForm');
	}


	public function updateUser($params)
	{
		$id = array_pop($params);

		if(!is_numeric($id))
		{
			$this->redirect('/');
			return;
		}

		$user = $this->loadModel('Users')->getUserBy(array(
			'id' => $id
		));

		if(!$user)
		{
			$this->redirect('/');
			return;
		}

		if(isset($_POST['updateuser']))
		{

			if(!isset($_POST['password']) || empty($_POST['password']))
			{
				$_POST['password'] = $user['password'];
			}
			$_POST['id'] = $id;

			$this->loadModel('users')->updateUser($_POST);

		} else {
			$_POST = $user;
		}

		$this->loadView('UpdateUserForm');
	}

	public function login()
	{
		$errors = array();


		if(isset($_POST['login']))
		{
			$username = null;
			$password = null;

			if(isset($_POST['username'])){
				$username = $_POST['username'];
			} else {
				$errors[]  = "Username";
			}

			if(isset($_POST['password'])){
				$password = $_POST['password'];
			} else {
				$errors[]  = "Password";
			}

			if(trim($username) == ""){
				$errors[]  = "Empty username";
			}

			if(trim($password) == ""){
				$errors[]  = "Empty password";
			}


			if(count($errors) === 0){
				$result = $this->loadModel('Users')->AuthenticateUser($username, $password);

				if($result){
					$_SESSION['logged'] = true;
					$_SESSION['loggedUser'] = $this->loadModel('users')->getUserByUsername($username);
					$this->redirect('/books/list');
				} else {
					$errors[] = 'invalid Username or Password';
				}
			}

		}


		$this->loadView('LoginForm', array('errors'=>$errors));
	}

	public function register()
	{
		$errors=array();

		if(isset($_POST['register'])){

			$user = $this->loadModel('Users')->getUserBy(array(
				'email' => $_POST['email']
			));

			if($user)
			{
				$errors[] = "Choose another email";
			}

			$user = $this->loadModel('Users')->getUserBy(array(
				'username' => $_POST['username']
			));
			if($user)
			{
				$errors[] = "Choose another username";
			}

			$user = $this->loadModel('Users')->getUserBy(array(
				'egn' => $_POST['egn']
			));
			if($user)
			{
				$errors[] = "Egn allready exists";
			}
			if(count($errors) === 0){

				$_POST['isadmin'] = 0;
				$this->loadModel('Users')->addUser($_POST);
			}
		}

		$this->loadView('RegisterForm');
	}
	public function logout(){
		if(session_destroy()) // Destroying All Sessions
		{
			$this->redirect('/'); // Redirecting To Home Page
		}
	}
	public function deleteUser($params){

		$id = array_pop($params);

		if(!is_numeric($id))
		{
			$this->redirect('/');
			return;
		}
		$user = $this->loadModel('Users')->getUserBy(array(
			'id' => $id
		));

		if(!$user)
		{
			$this->redirect('/');
			return;
		}

		$this->loadModel('users')->deleteUser($id);

		$this->redirect('/users/list');
	}

	public function getAll(){

		$result=$this->loadModel('Users')->GetAllUsers();


		$this->loadView('UsersList', ['res'=>$result]);
	}
}
