<?php


class BaseController {

	protected $config;
	
	private $models = array();

	public function __construct($config){
		$this->config = $config;
	}

	public function loadView($view, array $data = array())
	{

		$viewsPath = $this->config['engine']['basePath'].$this->config['engine']['views'];


		$data['header'] = $viewsPath."Header.php";
		$data['footer'] = $viewsPath."Footer.php";
		$data['view'] = $viewsPath.$view.".php";
		
		
		$data['logged'] = false;
		
		if(isset($_SESSION['logged']) && $_SESSION['logged'] === true)
		{
			$data['logged'] = true;
			$data['loggedUser'] = $_SESSION['loggedUser'];
		}
		
		extract($data);
		include($viewsPath."Layout.php");
	}
	public function loadModel($model){
		
		if( !isset($this->models[$model]) )
		{
			include_once($this->config['engine']['models'].'Model.php');
			include_once($this->config['engine']['models'].$model.'.php');
			
			$this->models[$model] = new $model($this->config);
			return $this->models[$model];
			
		} else {
			return $this->models[$model];
		}
	}

	public function redirect($url){
		header("Location: {$url}");
	}

}
