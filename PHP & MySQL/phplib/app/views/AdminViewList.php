<?php    ?>
<!--    Hover Rows  -->
<div class="panel panel-default">
    <div class="panel-heading">
        Users
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>EGN</th>
                        <th>Username</th>
                        <th>Password</th>
                        <th>FirstNameLastName</th>
                        <th>Phonenumber</th>
                        <th>Email</th>
                        <th>IsAsmin</th>

                        <th></th> <!-- Add -->
                        <th></th> <!-- Edit -->
                        <th></th> <!-- Delete -->
                    </tr>
                </thead>
                <tbody>
                    <?php
                        use ./Controller/UsersController;
                        GetAll();
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
 <!-- End  Hover Rows  -->


<!--    Hover Rows  -->
<div class="panel panel-default">
    <div class="panel-heading">
        Books
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Title</th>
                        <th>Author</th>
                        <th>Username</th>
                        <th>Price</th>
                        <th>Description</th>
                        <th>Picture</th>

                        <th></th> <!-- Add -->
                        <th></th> <!-- Edit -->
                        <th></th> <!-- Delete -->
                    </tr>
                </thead>
                <tbody>
                    <?php
                        use ./Controller/BooksController;
                        GetAll();
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
 <!-- End  Hover Rows  -->
