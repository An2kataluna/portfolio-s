<div class="row">
    <div class="col-md-6">
        <form method="post" accept-charset="utf-8">
        <?php if( is_array($errors) && count($errors) > 0 ){ ?>
            <?php foreach($errors as $err){ ?>
                <div class="alert alert-danger"><?php echo $err ?></div>
            <?php } ?>
        <?php } ?>

            <h4>  Adding User </h4>
            <br />
            <label>Enter Username : </label>
            <input type="text" class="form-control" name="username" /><br>
            <label>Enter Password :  </label>
            <input type="password" class="form-control" name="password" /><br>
            <label>Enter First Name and Last Name: </label>
            <input type="text" class="form-control" name="fnamelname" /><br>
            <label>Enter EGN: </label>
            <input type="number" class="form-control" name="egn" /><br>
            <label>Enter Email: </label>
            <input type="email" class="form-control" name="email" /><br>
            <label>Enter Phonenumber: </label>
            <input type="number" class="form-control" name="phonenumber" /><br>
            <!-- <label>Enter Admin Rights: </label>
            <input type="checkbox" class="form-control" name="isadmin" value="1"/> -->
            <hr />
            <button class="btn btn-info" name="adduser"><span class="glyphicon glyphicon-user"></span> &nbsp;Add User </button>&nbsp;
        </form>
    </div>
</div>
