<div class="col-md-24">
	 <!--    Hover Rows  -->
	<div class="panel panel-default">
		<div class="panel-heading">
			Users Table
		</div>
		<div class="panel-heading">
			<a href='/users/add'><span class='glyphicon glyphicon-plus'></span>Add User</a>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>EGN</th>
							<th>Username</th>
							<th>Password</th>
							<th>First Name and Last Name</th>
							<th>Email</th>
							<th>Phonenumber</th>
							<th>Admin Rights</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php
							foreach($res as $key=>$value){
								echo '<tr><td>'.$value['egn'].'</td>';
								echo '<td>'.$value['username'].'</td>';
								echo '<td>'.$value['password'].'</td>';
								echo '<td>'.$value['fnamelname'].'</td>';
								echo '<td>'.$value['email'].'</td>';
								echo '<td>'.$value['phonenumber'].'</td>';
								echo '<td>'.$value['isadmin'].'</td>';

									echo "<td><a href='/users/update/{$value['id']}'><span class='glyphicon glyphicon-pencil'></span>Edit</a> ";
									echo "<a href='/users/delete/{$value['id']}'><span class='glyphicon glyphicon-trash'></span>Delete</a></td>";
								echo '</tr>';
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<!-- End  Hover Rows  -->
</div>
