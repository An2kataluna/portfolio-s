<div class="row">
    <div class="col-md-6">
        <form action="" method="POST" accept-charset="utf-8">
            <h4>  Updating Book </h4>
            <br />
            <label>Title : </label>
            <input type="text" class="form-control" name="title" <?php if(isset($_POST['title'])){ echo "value='{$_POST['title']}'"; } ?>/><br>
            <label>Author :  </label>
            <input type="text" class="form-control" name="author" <?php if(isset($_POST['author'])){ echo "value='{$_POST['author']}'"; } ?>/><br>
            <label>Price: </label>
            <input type="number" class="form-control" name="price" <?php if(isset($_POST['price'])){ echo "value='{$_POST['price']}'"; } ?>/><br>
            <label>Description: </label>
			<textarea class="form-control" name="description" rows="15"><?php if(isset($_POST['description'])){ echo $_POST['description']; } ?></textarea>
            <label>Picture name: </label>
            <input type="file" class="form-control" name="cover"/>
			<select name="category">
                <option value="none" selected="selected">none</option>
                <option value="Fantasy">Fantasy</option>
				<option value="Romance">Romance</option>
				<option value="Thriller">Thriller</option>
				<option value="Horror">Horror</option>
				<option value="Erotic">Erotic</option>
				<option value="Fiction">Fiction</option>
			</select>
            <hr />
            <button class="btn btn-info" name="updatebook"><span class="glyphicon glyphicon-user"></span> &nbsp;Update Book </button>&nbsp;
        </form>
    </div>
</div>
