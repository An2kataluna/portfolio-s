    <section class="menu-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="navbar-collapse collapse ">
                    <ul id="menu-top" class="nav navbar-nav navbar-right">
						<?php if($logged){ ?>
							<li><a href="/books/list">Books</a></li><!-- class="menu-top-active"-->
							<?php if($loggedUser['isadmin'] == 1){ ?>
								<li><a href="/users/list">Users</a></li>
							<?php } ?>
						<?php } else { ?>
							<li><a href="/users/login">Login Now</a></li><!-- class="menu-top-active"-->
							<li><a href="/users/register">Register Now</a></li><!-- class="menu-top-active"-->						
						<?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>