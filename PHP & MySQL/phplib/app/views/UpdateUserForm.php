<div class="row">
    <div class="col-md-6">
        <form action="" method="POST" accept-charset="utf-8">
            <h4>  Updating User </h4>
            <br />
            <label>Username : </label>
            <input type="text" class="form-control" name="username" <?php if(isset($_POST['username'])){ echo "value='{$_POST['username']}'"; } ?> /><br>
            <label>Password :  </label>
            <input type="password" class="form-control" name="password" /><br>
            <label>First Name and Last Name: </label>
            <input type="text" class="form-control" name="fnamelname" <?php if(isset($_POST['fnamelname'])){ echo "value='{$_POST['fnamelname']}'"; } ?> /><br>
            <label>EGN: </label>
            <input type="number" class="form-control" name="egn" <?php if(isset($_POST['egn'])){ echo "value='{$_POST['egn']}'"; } ?> /><br>
            <label>Email: </label>
            <input type="email" class="form-control" name="email" <?php if(isset($_POST['email'])){ echo "value='{$_POST['email']}'"; } ?> /><br>
            <label>Phonenumber: </label>
            <input type="number" class="form-control" name="phonenumber" <?php if(isset($_POST['phonenumber'])){ echo "value='{$_POST['phonenumber']}'"; } ?> /><br>
            <label>Admin Rights: </label>
            <input type="checkbox" value=1 class="form-control" name="isadmin" <?php if(isset($_POST['isadmin']) && $_POST['isadmin'] == 1){ echo "checked"; } ?> />
            <hr />
            <button class="btn btn-info" name="updateuser"><span class="glyphicon glyphicon-user"></span> &nbsp;Update User </button>&nbsp;
        </form>
    </div>
</div>
