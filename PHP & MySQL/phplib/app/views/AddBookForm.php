<div class="row">
    <div class="col-md-6">
        <form action="" enctype="multipart/form-data" method="post" accept-charset="utf-8">

            <h4>  Adding Book </h4>
            <br />
            <label>Enter Title : </label>
            <input type="text" class="form-control" name="title" /><br>
            <label>Enter Author :  </label>
            <input type="text" class="form-control" name="author" /><br>
            <label>Enter Price: </label>
            <input type="number" class="form-control" name="price" /><br>
            <label>Enter Description: </label>
            <textarea class="form-control" name="description" rows="15"></textarea>

			 <label>Cover: </label>
            <input type="file" class="form-control" name="cover" />

			<label>Choose Categorie: </label>
			<select name="category">
				<option value="Fantasy">Fantasy</option>
				<option value="Romance">Romance</option>
				<option value="Thriller">Thriller</option>
				<option value="Horror">Horror</option>
				<option value="Erotic">Erotic</option>
				<option value="Fiction">Fiction</option>
			</select>
            <!-- <label>Enter Picture URL: </label>
            <input type="url" class="form-control" name="picture" /> -->
            <hr />
            <button class="btn btn-info" name="addbook"><span class="glyphicon glyphicon-user"></span> &nbsp;Add Book</button>&nbsp;
        </form>
    </div>
</div>
<?
