<?php if($loggedUser['isadmin'] == 1){
?>
	<div class="panel panel-default">
		<div class="panel-heading">
			<a href='/books/add'><span class='glyphicon glyphicon-plus'></span>Add Book</a>
		</div>
		<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Picture</th>
						<th>Title</th>
						<th>Author</th>
						<th>Description</th>
						<th>Category</th>
						<th>Price</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
			<?php   foreach($books as $key=>$values){
						foreach($values as $value){
						echo '<tr><td><image src="/content/images/'.$value['picture'].'"width="200"></td>';
						echo '<td>'.$value['title'].'</td>';
						echo '<td>'.$value['author'].'</td>';
						echo '<td>'.$value['description'].'</td>';
						echo '<td>'.$value['category'].'</td>';
						echo '<td>'.$value['price'].'</td>';

						echo "<td><a href='/books/update/{$value['id']}'><span class='glyphicon glyphicon-pencil'></span>Edit</a> ";
						echo "<a href='/books/delete/{$value['id']}'><span class='glyphicon glyphicon-trash'></span>Delete</a></td>";
						echo '</tr>';
						}
					}
					?>
				</tbody>
			</table>
		</div>
		</div>
	</div>
<?php }
 ?>

<div class="col-md-24">
	<div class="panel panel-default">
		<div class="panel-heading">
			Book Categories
		</div>
		<div class="panel-body">
			<ul class="nav nav-tabs">
			<?php $first = true; ?>
				<?php foreach($books as $key=>$book){ ?>
					<li <?php if($first) { echo "class='active'"; } ?>><a href="#<?php echo $key; ?>" data-toggle="tab"><?php echo $key; ?></a></li>
					<?php $first = false; ?>
				<?php } ?>
			</ul>

			<div class="tab-content">
			<?php $first = true; ?>
				<?php foreach($books as $key=>$values){ ?>

					<div class="tab-pane fade in <?php if($first) { echo "active"; } ?>" id="<?php echo $key ?>">
					<?php $first = false; ?>
					<h4><?php echo $key ?> Tab</h4>
						<?php foreach($values as $value) { ?>
							<?php echo'<div class="col-md-6 col-sm-6"><div class="panel panel-primary">';?>
								<div class="panel-heading">
									<image class="center-block" src="/content/images/<?php echo $value['picture'] ?>" width="200">
								</div>
								<?php
								echo '<div class="panel-body">';
								echo'<B>Title:</b> '.$value['title']."<br>";
								echo'<B>Author:</b> '.$value['author']."<br>";
								echo'<b>Description:</b> '.$value['description']."<br>";
								echo'<b>Category:</b> '.$value['category']."<br>";
								echo'</div><div class="panel-footer">'."<br>";
								echo'<b>Price:</b> '.$value['price']."$ <br>";
								echo'</div></div></div>';
								?>
						<?php } ?>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
