<div class="row">
    <div class="col-md-6">
        <form action="" method="post" accept-charset="utf-8">
            <h4>  Register User</h4>
            <br />
            <label>Enter Username : </label>
            <input type="text" class="form-control" name="username" /><br>
            <label>Enter Password :  </label>
            <input type="password" class="form-control" name="password" /><br>
            <label>Confirm Password :  </label>
            <input type="password" class="form-control" name="confirm-password" /><br>
            <label>Enter First Name and Last Name: </label>
            <input type="text" class="form-control" name="fnamelname" /><br>
            <label>Enter EGN: </label>
            <input type="number" class="form-control" name="egn" /><br>
            <label>Enter Email: </label>
            <input type="email" class="form-control" name="email" /><br>
            <label>Enter Phonenumber: </label>
            <input type="number" class="form-control" name="phonenumber" />
            <hr />
            <button type="" class="btn btn-info" name="register"><span class="glyphicon glyphicon-user"></span> &nbsp;Register user</button>&nbsp;
        </form>
    </div>
</div>
