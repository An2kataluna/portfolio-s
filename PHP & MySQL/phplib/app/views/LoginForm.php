<div class="row">
    <div class="col-md-6">
        <form method="post">
			<?php if( is_array($errors) && count($errors) > 0 ){ ?>
				<?php foreach($errors as $err){ ?>
					<div class="alert alert-danger"><?php echo $err ?></div>
				<?php } ?>
			<?php } ?>
            <h4>  Login </h4>
            <br />
            <label>Enter Username : </label>
            <input type="text" class="form-control" name="username" <?php if(isset($_POST['username'])){ echo "value={$_POST['username']}" ;} ?> /><br>
            <label>Enter Password :  </label>
            <input type="password" class="form-control" name="password" />
            <hr />
            <button type="submit" class="btn btn-info" name="login"><span class="glyphicon glyphicon-user"></span> &nbsp;Log Me In </button>&nbsp;
            <!-- <a href="index.html" class="btn btn-info"><span class="glyphicon glyphicon-user"></span> &nbsp;Log Me In </a>&nbsp; -->
        </form>
    </div>
</div>


