<?php

class Engine {
	
	public function run($config)
	{
		
		$requestedPage = $_SERVER['REQUEST_URI'];
		$pages = $config['paths'];
		$params = array(); 
		if($requestedPage != '/')
		{			
			foreach($pages as $url=>$page)
			{	
				$url = trim($url, '/');
				if(!empty($url))
				{
					if(strpos(trim($requestedPage, '/'), $url) !== false)
					{
						$pageConfig = $page;
						$params = explode('/', trim($requestedPage, $url));
					}
				}

				
			}
		} else
		{
			$pageConfig = $pages[$requestedPage];
		}
		
		include_once($config['engine']['basePath'].$config['engine']['controllers'].'BaseController'.".php");
		include_once($config['engine']['basePath'].$config['engine']['controllers'].$pageConfig['class'].".php");
		
		$class = new $pageConfig['class']($config);
		$class->$pageConfig['method']($params);
		
		
	}

}