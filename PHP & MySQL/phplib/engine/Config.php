<?php

return array(
	'mysql' => array(
		'host' => 'localhost',
		'user'=> 'root',
		'password'=>'',
		'db' => 'onlinelibrary2'
	),
	'engine' => array(
		'controllers' => 'app'.DIRECTORY_SEPARATOR.'controllers'.DIRECTORY_SEPARATOR,
		'models' => 'app'.DIRECTORY_SEPARATOR.'models'.DIRECTORY_SEPARATOR,
		'views' => 'app'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR,
		'basePath' => realpath(__DIR__ . DIRECTORY_SEPARATOR . '..').DIRECTORY_SEPARATOR
	),
	'paths' => array(
		'/users/add' => array( 'class' => 'UsersController', 'method' => 'addUser' ),
		'/users/update' => array( 'class' => 'UsersController', 'method' => 'updateUser' ),
		'/users/delete' => array( 'class' => 'UsersController', 'method' => 'deleteUser' ),
		'/users/list' => array( 'class' => 'UsersController', 'method' => 'getAll' ),
		'/books/add' => array( 'class' => 'BooksController', 'method' => 'addBook' ),
		'/books/update'=> array( 'class' => 'BooksController', 'method' => 'updateBook' ),
		'/books/delete'=> array( 'class' => 'BooksController', 'method' => 'deleteBook' ),
		'/books/list' => array( 'class' => 'BooksController', 'method' => 'getAll' ),
		'/'=> array( 'class' => 'UsersController', 'method' => 'login' ),
		'/users/login'=> array( 'class' => 'UsersController', 'method' => 'login' ),
		'/users/register'=> array( 'class' => 'UsersController', 'method' => 'register' ),
		'/users/logout'=> array( 'class' => 'UsersController', 'method' => 'logout' )
	)
	
);

