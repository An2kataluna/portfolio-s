<?php
/*
Single Post Template: Blog Post
Description: This part is optional, but helpful for describing the Post Template
*/
?>

<?php get_header(); ?>

<div id="post-top" class="col s12 tags-bg hide-on-med-and-down"></div>

<section class="section secondary-section-bg">
    <div class="row container">
        <div class="col s12 m8 l8 left white">

<?php while ( have_posts() ) : the_post(); ?>

            <div class="post col s12">

                <h4 class="col s12 single-post-title"><?php the_title(); ?></h4>

                <?php if( has_post_thumbnail()): ?>
                    <?php the_post_thumbnail('', array('class' => 'col s12')); ?>
                <?php endif; ?>

                <div class="col s12 post-info">
                    <span class="left-align"><span class="bolded">Posted in: </span><?php the_category( ', ' ); ?></span>
                    <span class="left-align"><span class="bolded">Posted by: </span><?php the_author(); ?> </span>
                    <span class="right"><i class="zmdi zmdi-hc-fw"></i><?php comments_number(); ?> </span>
                    <span class="right"><i class="zmdi zmdi-hc-fw"></i><?php echo do_shortcode( '[post_view time="day"]' ) ?></span>
                </div>

                <div class="col s12 post-text">
                    <?php the_content(); ?>

                    <p><?php edit_post_link(); ?></p>
                </div>

                <div class="tags">

                <?php
                  if(has_tag()) {
                ?>

                <span class="left-align"><span class="bolded">Tags: </span><?php the_tags('','',''); ?></span>

                <?php
                  } else {
                      //Article untagged
                  }
                ?>

                </div>

            </div>

<?php endwhile; ?>
        </div>

        <!-- Sidebar Here -->
        <div class="col s12 m3 l3 right">
            <?php dynamic_sidebar( 'sidebar' ); ?>
        </div>
    </div>
</section>

<section class="section form-bg">
    <div class="row container">
        <div class="col s12">
            <h4 class="consultation-title center-align">Sign up for free consultation</h4>
            <?php echo do_shortcode( '[contact-form-7 id="64" title="Contact form 1"]' ); ?>
            <!-- <form action="" method="post" class="consult-form">
              <div class="row">
                <div class="input-field col s12 m4 l4">
                  <label for="name">Name *</label>
                  <input name="Name" type="text" id="name"  class="validate" required>
                </div>
                <div class="input-field col s12 m4 l4">
                  <label for="phone">Phone Number *</label>
                  <input id="phone" type="tel" class="validate" name="Phone">
                </div>
                <div class="input-field col s12 m4 l4">
                  <label for="email">Email *</label>
                  <input id="email" type="email" class="validate" name="Email" required>
                </div>
              </div>
              <div class="row">
                <div class="col s12">
                  <label for="comment">Your Comment</label>
                  <textarea id="comment" class="validate" name="Message" rows="5" required></textarea>
                </div>
              </div>
              <div class="row">
                <div class="col s12">
                  <img src="../wp-content/plugins/html-contact-form/captcha.php">
                    <input name="captcha" placeholder="Enter captcha text here" type="text">
                </div>
              </div>
              <div class="center-align">
                <button type="submit" class="waves-effect waves-light btn send">Send Form</button>
              </div>

            </form> -->
        </div>
    </div>
</section>

<?php get_footer(); ?>