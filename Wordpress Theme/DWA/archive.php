<?php
    /**
        * The template for displaying blog archive pages
        *
        * Used to display archive-type pages if nothing more specific matches a query.
        * For example, puts together date-based pages if no date.php file exists.
        *
        * If you'd like to further customize these archive views, you may create a
        * new template file for each one. For example, tag.php (Tag archives),
        * category.php (Category archives), author.php (Author archives), etc.
        *
        * @link https://codex.wordpress.org/Template_Hierarchy
        *
        * @package WordPress
        * @subpackage DWA
        * @since DWA 1.0
    */

get_header(); ?>

<!-- Content -->

<div id="blog" class="parallax-container flex-centering">
    <div class="valign center-align">
        <p class="blog-descr white-text">Our Blog</p>
        <h1 class="parallax-title center white-text main-title">
            OUR LATEST NEWS
        </h1>
        <p class="blog-descr white-text">Home / Blog</p>
    </div>

    <div class="parallax">
        <img src="<?php echo esc_url( get_theme_mod( 'blog_parallax_image' ) ); ?>">
    </div>
</div>

<section class="section secondary-section-bg">
    <div class="row container">
        <div class="col s12 m8 l8 left white">

<?php
// the query to set the posts per page to 3
//
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
    'posts_per_page' => 3,
    'paged' => $paged ,
    'has_archive' => false
    );
query_posts($args);
?>

    <?php if ( have_posts() ) : while (have_posts()) : the_post(); ?>

        <!-- ANY OUTPUT FOR ASIDE POSTS -->
            <?php if( get_post_format() == 'aside' ) : ?>
            <div class="post col s12">
                <div class="col s12">
                    <?php the_content(); ?>
                </div>
                <div class="read-more">
                    <a href="<?php the_permalink(); ?>" class="waves-effect waves-light btn secondary-bg">Read more</a>
                </div>
                <div class="divider"></div>
            </div>

        <!-- ANY OUTPUT FOR VIDEO POSTS -->
            <?php elseif( get_post_format() == 'video' ) : ?>
            <div class="post col s12">
                <div class="image-wrapper">

                    <?php the_content(); ?>
                </div>
                <div class="col s12">
                    <span class="left-align"><span class="bolded">Posted in: </span><?php the_category( ', ' ); ?></span>
                    <span class="left-align"><span class="bolded">Posted by: </span><?php the_author(); ?> </span>
                    <!-- <span class="left-align"><span class="bolded">Tags: </span><?php // the_tags( '', ', ', '' ); ?></span> -->

                    <span class="right"><i class="zmdi zmdi-hc-fw"></i><?php comments_number(); ?> </span>
                    <span class="right"><i class="zmdi zmdi-hc-fw"></i><?php echo do_shortcode( '[post_view time="day"]' ) ?></span>
                </div>
                <div class="read-more">
                    <a href="<?php the_permalink(); ?>" class="waves-effect waves-light btn secondary-bg">Read more</a>
                </div>

            </div>

        <!-- ANY OUTPUT FOR IMAGE POSTS -->
            <?php elseif( get_post_format() == 'image' ) : ?>
            <div class="post col s12">
                <div class="image-wrapper">

                    <?php the_content(); ?>
                </div>
                <div class="col s12">
                    <span class="left-align"><span class="bolded">Posted in: </span><?php the_category( ', ' ); ?></span>
                    <span class="left-align"><span class="bolded">Posted by: </span><?php the_author(); ?> </span>
                    <!-- <span class="left-align"><span class="bolded">Tags: </span><?php // the_tags( '', ', ', '' ); ?></span> -->

                    <span class="right"><i class="zmdi zmdi-hc-fw"></i><?php comments_number(); ?> </span>
                    <span class="right"><i class="zmdi zmdi-hc-fw"></i><?php echo do_shortcode( '[post_view time="day"]' ) ?></span>
                </div>
                <div class="read-more">
                    <a href="<?php the_permalink(); ?>" class="waves-effect waves-light btn secondary-bg">Read more</a>
                </div>
                <div class="divider"></div>
            </div>

        <!-- ANY OUTPUT FOR QUOTE POSTS -->
            <?php elseif( get_post_format() == 'quote' ) : ?>
            <div class="post col s12">
                <div class="col s12">
                    <span class="left-align"><span class="bolded">Posted in: </span><?php the_category( ', ' ); ?></span>
                    <span class="left-align"><span class="bolded">Posted by: </span><?php the_author(); ?> </span>
                    <!-- <span class="left-align"><span class="bolded">Tags: </span><?php // the_tags( '', ', ', '' ); ?></span> -->

                    <span class="right"><i class="zmdi zmdi-hc-fw"></i><?php comments_number(); ?> </span>
                    <span class="right"><i class="zmdi zmdi-hc-fw"></i><?php echo do_shortcode( '[post_view time="day"]' ) ?></span>
                </div>
                <div class="col s12 post-info">
                    <?php the_content(); ?>
                </div>
                <div class="read-more">
                    <a href="<?php the_permalink(); ?>" class="waves-effect waves-light btn secondary-bg">Read more</a>
                </div>
                <div class="divider"></div>
            </div>

        <!-- ANY OUTPUT FOR LINK POSTS -->
            <?php elseif( get_post_format() == 'link' ) : ?>
            <div class="post col s12">
                <blockquote>
                    <?php the_content(); ?>
                </blockquote>
                <div class="col s12">
                    <span class="left-align"><span class="bolded">Posted in: </span><?php the_category( ', ' ); ?></span>
                    <span class="left-align"><span class="bolded">Posted by: </span><?php the_author(); ?> </span>
                    <!-- <span class="left-align"><span class="bolded">Tags: </span><?php // the_tags( '', ', ', '' ); ?></span> -->

                    <span class="right"><i class="zmdi zmdi-hc-fw"></i><?php comments_number(); ?> </span>
                    <span class="right"><i class="zmdi zmdi-hc-fw"></i><?php echo do_shortcode( '[post_view time="day"]' ) ?></span>
                </div>
                <div class="read-more">
                    <a href="<?php the_permalink(); ?>" class="waves-effect waves-light btn secondary-bg">Read more</a>
                </div>
                <div class="divider"></div>
            </div>

        <!-- ANY OUTPUT FOR GALLERY POSTS -->
            <?php elseif( get_post_format() == 'gallery' ) : ?>
            <div class="post col s12">
                <div class="col s12">
                    <?php the_content(); ?>
                </div>
                <div class="col s12 post-info">
                    <span class="left-align"><span class="bolded">Posted in: </span><?php the_category( ', ' ); ?></span>
                    <span class="left-align"><span class="bolded">Posted by: </span><?php the_author(); ?> </span>
                    <!-- <span class="left-align"><span class="bolded">Tags: </span><?php // the_tags( '', ', ', '' ); ?></span> -->

                    <span class="right"><i class="zmdi zmdi-hc-fw"></i><?php comments_number(); ?> </span>
                    <span class="right"><i class="zmdi zmdi-hc-fw"></i><?php echo do_shortcode( '[post_view time="day"]' ) ?></span>
                </div>
                <div class="read-more">
                    <a href="<?php the_permalink(); ?>" class="waves-effect waves-light btn secondary-bg">Read more</a>
                </div>
                <div class="divider"></div>
            </div>

        <!-- ANY OUTPUT FOR AUDIO POSTS -->
            <?php elseif( get_post_format() == 'audio' ) : ?>
            <div class="post col s12">
                <div class="image-wrapper">
                    <?php the_content(); ?>
                </div>
                <div class="col s12">
                    <span class="left-align"><span class="bolded">Posted in: </span><?php the_category( ', ' ); ?></span>
                    <span class="left-align"><span class="bolded">Posted by: </span><?php the_author(); ?> </span>
                    <!-- <span class="left-align"><span class="bolded">Tags: </span><?php // the_tags( '', ', ', '' ); ?></span> -->

                    <span class="right"><i class="zmdi zmdi-hc-fw"></i><?php comments_number(); ?> </span>
                    <span class="right"><i class="zmdi zmdi-hc-fw"></i><?php echo do_shortcode( '[post_view time="day"]' ) ?></span>
                </div>
                <div class="read-more">
                    <a href="<?php the_permalink(); ?>" class="waves-effect waves-light btn secondary-bg">Read more</a>
                </div>

                <div class="divider"></div>
            </div>

        <!-- ANY OUTPUT FOR STANDARD POSTS -->
            <?php else : ?>
            <div class="post col s12">
                <div class="image-wrapper">
                    <span class="date-sticker secondary-bg">
                        <span class="month white-text"><?php the_time( 'M' ); ?></span>
                        <span class="day white-text"><?php the_time( 'd' ); ?></span>
                    </span>
                    <?php if( has_post_thumbnail()): ?>
                        <?php the_post_thumbnail('', array('class' => 'col s12')); ?>
                    <?php else: ?>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/default-post-image.png" alt="" class="col s12">
                    <?php endif; ?>
                </div>
                <div class="col s12 post-info">
                    <span class="left-align"><span class="bolded">Posted in: </span><?php the_category( ', ' ); ?></span>
                    <span class="left-align"><span class="bolded">Posted by: </span><?php the_author(); ?> </span>
                    <!-- <span class="left-align"><span class="bolded">Tags: </span><?php // the_tags( '', ', ', '' ); ?></span> -->

                    <span class="right"><i class="zmdi zmdi-hc-fw"></i><?php comments_number(); ?> </span>
                    <span class="right"><i class="zmdi zmdi-hc-fw"></i><?php echo do_shortcode( '[post_view time="day"]' ) ?></span>
                </div>
                <h2 class="col s12 post-title"><?php the_title(); ?></h2>
                <div class="col s12 post-text"><?php echo mb_substr(get_the_excerpt(), 0,350)."..."; ?></div>
                <div class="read-more">
                    <a href="<?php the_permalink(); ?>" class="waves-effect waves-light btn secondary-bg">Read more</a>
                </div>

                <div class="divider"></div>
            </div>

            <?php endif; ?>


        <?php endwhile; ?>

        <!-- pagination -->

        <div class="row">

            <div class="col s12">
                <div class="left">
                    <?php previous_posts_link(); ?>
                </div>

                <div class="right">
                    <?php next_posts_link(); ?>
                </div>
            </div>
        </div>


    <?php  else : ?>
        <!-- No posts found -->

    <?php endif; ?>

        </div>

        <!-- Sidebar Here -->
        <div class="col s12 m3 l3 right">
            <?php dynamic_sidebar( 'sidebar' ); ?>
        </div>
    </div>
</section>

<!-- /Content -->

<?php get_footer(); ?>