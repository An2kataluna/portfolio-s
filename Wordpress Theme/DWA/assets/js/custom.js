$( document ).ready(function(){

/**
 * Home Page Components
 */
// Anchor Links to Section

    $(function() {
      $('.top-menu a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html, body').animate({
              scrollTop: target.offset().top
            }, 1000);
            return false;
          }
        }
      });
    });


// Anchor Links to Top
    //Click event to scroll to top
    $('.scroll-top').click(function(){
        $('html, body').animate({scrollTop : 0},800);
        return false;
    });

    $('.scroll-top').fadeOut();

    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('.scroll-top').fadeIn();
        } else {
            $('.scroll-top').fadeOut();
        }
    });


// Navigation Initialize
    $(".button-collapse").sideNav();

// Paralx Initialize
    $('#home .parallax').parallax();


// Carousel About Us Initialize

    $('#about-us .about-us-container.carousel.carousel-slider').carousel({
        full_width: true,
        indicators: true
    }).height( 200 );


// Carousel Partners Initialize

    $('#partners .partners-container.carousel.carousel-slider').carousel({
        full_width: true,
        indicators: true
    }).height( 170 );


// Directions Carousel
    $('#directions ul.tabs').tabs();


/**
 * Blog Page Components
 */
// Paralx Initialize
    $('#blog .parallax').parallax();


/**
 * 404 Page Components
 */
// Paralx Initialize
    $('#page-404 .parallax').parallax();


})