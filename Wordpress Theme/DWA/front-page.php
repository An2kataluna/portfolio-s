<?php get_header(); ?>

<!-- Content -->

    <div id="home" class="parallax-container">

        <h1 class="parallax-title center white-text">
            Legal Assistance <br> in all matters
        </h1>

        <div class="parallax">
            <img src="<?php echo esc_url( get_theme_mod( 'home_parallax_image' ) ); ?>">
        </div>
    </div>

    <section class="primary-bg">
        <div class="container">
            <div class="row">
                <div class="col white-text l4 s12">
                    <p class="center-align stat-numbers">
                        <?php echo get_theme_mod( 'statistics1_number' ); ?>
                    </p>
                    <p class="center-align stat-descr">
                        <?php echo get_theme_mod( 'statistics1_text' ); ?>
                    </p>
                </div>
                <div class="col white-text l4 s12">
                    <p class="center-align stat-numbers">
                        <?php echo get_theme_mod( 'statistics2_number' ); ?>%
                    </p>
                    <p class="center-align stat-descr">
                        <?php echo get_theme_mod( 'statistics2_text' ); ?>
                    </p>
                </div>
                <div class="col white-text l4 s12">
                    <p class="center-align stat-numbers">
                        <?php echo get_theme_mod( 'statistics3_number' ); ?>
                    </p>
                    <p class="center-align stat-descr">
                        <?php echo get_theme_mod( 'statistics3_text' ); ?>
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section id="about-us" name="about-us" class="main-section-bg">
        <div class="section center separator">
            <i class="material-icons primary-color icon-size-mid">hdr_strong</i>
            <i class="material-icons primary-color icon-size-mid">hdr_weak</i>
            <i class="material-icons primary-color icon-size-mid">hdr_weak</i>
            <i class="material-icons primary-color icon-size-mid">hdr_strong</i>
        </div>

        <div class="container">

            <div class="row z-depth-3">
                <div class="col s12 l5 right">
                  <img src="<?php echo esc_url( get_theme_mod( 'about_image' ) ); ?>" class="carousel-img">
                </div>
                <div class="col s12 l7 left">

                    <h2 class="about-us-title"><?php echo get_theme_mod( 'about_title' ); ?></h2>

                    <div class="carousel carousel-slider about-us-container" data-indicators="true">
                        <div class="carousel-item" href="#one!">
                            <span><?php echo get_theme_mod( 'about_info_slide1' ); ?></span>
                        </div>
                        <div class="carousel-item" href="#two!">
                            <span><?php echo get_theme_mod( 'about_info_slide2' ); ?></span>
                        </div>
                        <div class="carousel-item" href="#three!">
                            <span><?php echo get_theme_mod( 'about_info_slide3' ); ?></span>
                        </div>
                        <div class="carousel-item" href="#four!">
                            <span><?php echo get_theme_mod( 'about_info_slide3' ); ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section center separator">
            <i class="material-icons primary-color icon-size-mid">hdr_strong</i>
            <i class="material-icons primary-color icon-size-mid">hdr_weak</i>
            <i class="material-icons primary-color icon-size-mid">hdr_weak</i>
            <i class="material-icons primary-color icon-size-mid">hdr_strong</i>
        </div>
    </section>

    <section id="latest-news" class="secondary-section-bg">
        <h4 class=""><a href="<?php echo esc_url( home_url( '/' ) ).esc_html__( 'news', 'textdomain' ); ?>">Latest News</a></h4>
        <div class="row news-wrapper">
            <div class="row scroll-wrapper">

            <?php
              query_posts( array( 'post_type' => 'post', 'posts_per_page' => 6) );
              if ( have_posts() ) : while ( have_posts() ) : the_post();
            ?>

                <div class="news-item white">
                    <?php if( has_post_thumbnail()): ?>
                        <?php the_post_thumbnail(); ?>
                    <?php else: ?>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/default-post-image.png" alt="" class="col s12">
                    <?php endif; ?>
                    <h5><a href="<?php the_permalink(); ?>"><?php if ( get_the_title() ) the_title(); else the_ID(); ?></a></h5>
                    <p><?php echo mb_substr(get_the_excerpt(), 0,150)."..."; ?></p>
                </div>

            <?php endwhile; endif; wp_reset_query(); ?>

            </div>
        </div>
    </section>

    <section id="directions" class="container main-section-bg">

        <div class="section center separator">
            <i class="material-icons primary-color icon-size-mid">hdr_strong</i>
            <i class="material-icons primary-color icon-size-mid">hdr_weak</i>
            <i class="material-icons primary-color icon-size-mid">hdr_weak</i>
            <i class="material-icons primary-color icon-size-mid">hdr_strong</i>
        </div>

        <div class="row z-depth-3">
            <div class="directions-wrapper">
                <h4 class="col s6 left directions-title">Directions</h4>
                <div class="col s6 right">
                    <ul class="tabs">
                        <li class="tab col s3">
                            <a href="#tab1" class="black-text">
                                <?php echo get_theme_mod( 'tab_title1' ); ?>
                            </a>
                        </li>
                        <li class="tab col s3">
                            <a href="#tab2" class="black-text">
                                <?php echo get_theme_mod( 'tab_title2' ); ?>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row directions-content">
                <div id="tab1" class="col s12">
                    <?php echo get_theme_mod( 'directions_tab1' ); ?>
                </div>
                <div id="tab2" class="col s12">
                    <?php echo get_theme_mod( 'directions_tab2' ); ?>
                </div>
            </div>
        </div>

        <div class="section center separator">
            <i class="material-icons primary-color icon-size-mid">hdr_strong</i>
            <i class="material-icons primary-color icon-size-mid">hdr_weak</i>
            <i class="material-icons primary-color icon-size-mid">hdr_weak</i>
            <i class="material-icons primary-color icon-size-mid">hdr_strong</i>
        </div>
    </section>

    <section id="partners" class="secondary-section-bg">
        <h4 class="partners-title center">Our Partners</h4>
        <div class="carousel carousel-slider partners-container">
            <div class="carousel-item center" href="#one!">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo1.png" alt="" class="partner-logo">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo2.png" alt="" class="partner-logo">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo3.png" alt="" class="partner-logo">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo4.png" alt="" class="partner-logo">
            </div>
            <div class="carousel-item center" href="#two!">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo5.png" alt="" class="partner-logo">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo6.png" alt="" class="partner-logo">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo7.png" alt="" class="partner-logo">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo8.png" alt="" class="partner-logo">
            </div>
            <div class="carousel-item center" href="#three!">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo9.png" alt="" class="partner-logo">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo10.png" alt="" class="partner-logo">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo11.png" alt="" class="partner-logo">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo12.png" alt="" class="partner-logo">
            </div>
            <div class="carousel-item center" href="#four!">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo13.png" alt="" class="partner-logo">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo14.png" alt="" class="partner-logo">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo15.png" alt="" class="partner-logo">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo16.png" alt="" class="partner-logo">
            </div>
        </div>
    </section>

<!-- /Content -->

<?php get_footer(); ?>