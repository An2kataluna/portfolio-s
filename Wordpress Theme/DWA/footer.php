<footer id="footer" class="white-text">
        <div class="primary-bg footer-wrapper">
            <div class="container row">

                <div class="col s12 m4 l4 center-align company-info">
                    <?php dynamic_sidebar( 'footer-fc' ); ?>
                </div>

                <div class="col s12 m5 l5 center-align">
                    <?php dynamic_sidebar( 'footer-sc' ); ?>
                </div>

                <div class="col s12 m3 l3 center-align contact-info">
                    <?php dynamic_sidebar( 'footer-tc' ); ?>
                </div>

                <div class="footer-logo center-align">
                    <a href="<?php echo get_home_url(); ?>" class="center brand-logo">
                        <?php

                            if (get_theme_mod( 'dwa_logo' )) {
                                echo "<img src='".esc_url( get_theme_mod( 'dwa_logo' ) )."'>";
                            } else {
                                echo get_theme_mod( 'logo_code' );
                            }

                        ?>
                    </a>
                </div>
            </div>
        </div>
    </footer>

    <div class="scroll-top">
        <a href="#top" class="center-align">
            <i class="material-icons medium primary-color">keyboard_arrow_up</i>
        </a>
    </div>

    <!-- Scripts -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/materialize.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/custom.js"></script>
    <?php // wp_footer() ?>

</body>
</html>