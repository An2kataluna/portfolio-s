<?php get_header(); ?>

<!-- Content -->

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <div id="post-top" class="col s12 tags-bg hide-on-med-and-down"></div>

    <?php the_content(); ?>

<?php endwhile; endif; ?>


<!-- /Content -->
<?php get_footer(); ?>