<?php

/**
* DWA functions and definitions
*
* Set up the theme and provides some helper functions, which are used in the
* theme as custom template tags. Others are attached to action and filter
* hooks in WordPress to change core functionality.
*
* When using a child theme you can override certain functions (those wrapped
* in a function_exists() call) by defining them first in your child theme's
* functions.php file. The child theme's functions.php file is included before
* the parent theme's file, so the child theme functions would be used.
*
* @link https://codex.wordpress.org/Theme_Development
* @link https://codex.wordpress.org/Child_Themes
*
* Functions that are not pluggable (not wrapped in function_exists()) are
* instead attached to a filter or action hook.
*
* For more information on hooks, actions, and filters,
* {@link https://codex.wordpress.org/Plugin_API}
*
* @package WordPress
* @subpackage DWA
* @since DWA 1.0
*/


function dwa_setup(){

    load_theme_textdomain( 'DWA', get_template_directory() . '/languages' );

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support( 'title-tag' );


    /*
     * Enable support for custom logo.
     */
    add_theme_support( 'custom-logo', array(

        'height'      => 110,
        'width'       => 340,
        'flex-height' => true,

    ) );


    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
     */
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 'full' );


    // This theme uses wp_nav_menu() in one location.
    register_nav_menus( array(
        'primary' => __( 'Primary Menu', 'DWA' ),
        'social'  => __( 'Social', 'DWA' ),
    ) );


    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */

    // add_theme_support( 'html5', array(
    //     'search-form',
    //     'comment-form',
    //     'comment-list',
    //     'gallery',
    //     'caption',
    // ) );


    /*
     * Enable support for Post Formats.
     *
     * See: https://codex.wordpress.org/Post_Formats
     */
    add_theme_support( 'post-formats', array(

        'aside',
        'image',
        'video',
        'quote',
        'link',
        'gallery',
        'status',
        'audio',
        'chat',

    ) );

    // Indicate widget sidebars can use selective refresh in the Customizer.
    add_theme_support( 'customize-selective-refresh-widgets' );

} // end dwa-setup

add_action( 'after_setup_theme', 'dwa_setup' );


// Adding custom button in navigation
function add_last_nav_item($items) {
    $text = "/contact/";
  return $items .= '<li><a href="'. home_url( $text ) .'" class="waves-effect waves-light btn secondary-bg">Free Consultation</a></li>';
}
add_filter('wp_nav_menu_items','add_last_nav_item');

/**
 * Enqueues scripts and styles.
 *
 * @since DWA 1.0
 */

function dwa_scripts_end_styles() {

 // Theme stylesheets

    /*wp_enqueue_style( 'material-icons', get_template_directory_uri() . './assets/css/material-design-iconic-font.min.css', array(), '3.4.1', true );
    wp_enqueue_style( 'materialize-css', get_template_directory_uri() . '/assets/css/materialize.min.css', array(), '3.4.1', true );
    wp_enqueue_style( 'custom-css',  get_template_directory_uri(). '/assets/css/custom.css',
    array(), '3.4.1', true );*/

 // Scripts included in footer
    /*wp_enqueue_script( 'materialize-js',  get_template_directory_uri(). '/assets/js/materialize.min.js', array(), '3.4.1', false );
    wp_enqueue_script( 'custom-js',  get_template_directory_uri(). '/assets/js/custom.js',array(), '3.4.1', false );*/

} // end dwa_scripts_end_styles

add_action( 'wp_enqueue_scripts', 'dwa_scripts_end_styles' );


/**
 * Registers a widget area.
 *
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 *
 * @since DWA 1.0
 */

function dwa_widgets_init() {

// Sidebar Area
    register_sidebar( array(

        'name'          => __( 'Sidebar', 'DWA' ),
        'id'            => 'sidebar',
        'description'   => __( 'Add widgets here to appear in your sidebar in Blog page.', 'DWA' ),
        'before_widget' => '<div id="%1$s" class="section">',
        'after_widget'  => '</div>',
        'before_title'  => '<h5 class="sidebar-title">',
        'after_title'   => '</h5>',

    ) );

// Footer Area
    register_sidebar( array(

        'name'          => __( 'Footer-First-Column', 'DWA' ),
        'id'            => 'footer-fc',
        'description'   => __( 'Add widgets here to appear in your Footer.', 'DWA' ),
        'before_widget' => '<div id="%1$s" class="company-descr col s12 secondary-text-color">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="left-align">',
        'after_title'   => ' </h4>'

    ) );

    register_sidebar( array(

        'name'          => __( 'Footer-Second-Column', 'DWA' ),
        'id'            => 'footer-sc',
        'description'   => __( 'Add widgets here to appear in your Footer.', 'DWA' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h4 class="left-align">',
        'after_title'   => ' </h4>'

    ) );

    register_sidebar( array(

        'name'          => __( 'Footer-Third-Column', 'DWA' ),
        'id'            => 'footer-tc',
        'description'   => __( 'Add widgets here to appear in your Footer.', 'DWA' ),
        'before_widget' => '<div id="%1$s" class="secondary-text-color right-align">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="left-align">',
        'after_title'   => ' </h4>'

    ) );

} // end dwa_widgets_init

add_action( 'widgets_init', 'dwa_widgets_init' );



/**
 * Proper way to add a logo uploader in your theme
 * @param  [Object] $wp_customize adds admin opions in Theme Customizer
 *
 */
function dwa_theme_customizer( $wp_customize ){

    /**
     * Set default layout of theme customizer
     */


    /**
     * Adding Panels
     */
    $wp_customize->add_panel( 'dwa_pages_options', array( // Pages Panel
        'priority'       => 105,
        'capability'     => 'edit_theme_options',
        'title'          => __('Home Page Options', 'DWA'),
        'description'    => __('Deals with the home page of your theme.', 'DWA')
    ));
    $wp_customize->add_panel( 'dwa_blog_page_options', array( // Pages Panel
        'priority'       => 106,
        'capability'     => 'edit_theme_options',
        'title'          => __('Blog Page Options', 'DWA'),
        'description'    => __('Deals with the blog page of your theme.', 'DWA')
    ));


    /**
     * Sections , Settings and Controls
     */

    // Logo
        $wp_customize->add_section( 'dwa_logo_section' , array(
        'title'       => __( 'Logo', 'DWA' ),
        'priority'    => 30,
        'description' => 'Upload a logo to replace the default site name and description in the header',
        ) );
        $wp_customize->add_setting( 'dwa_logo' );
        $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'dwa_logo', array(
        'label'    => __( 'Logo', 'DWA' ),
        'section'  => 'dwa_logo_section',
        'settings' => 'dwa_logo',
        ) ) );
        // or textarea for custom code
        $wp_customize->add_setting('logo_code', array(
            'default'      => '',
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
            'transport'    => 'postMessage',
            'priority' => 15
            )
        );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'logo_code', array(
                'type' => 'textarea',
                'label'    => 'Logo custom code',
                'settings' => 'logo_code',
                'section'  => 'dwa_logo_section'
                )
            )
        );

    // Home page

        // Top Text
        $wp_customize->add_section('top_info_options', array(
            'title'       => __('Top Info in Home page', 'DWA'),
            'description' => __('Upload Top Info in home page', 'DWA'),
            'priority'    => 201,
            'panel'       => 'dwa_pages_options'
            )
        );
        // Text {1}
            $wp_customize->add_setting('top_info1', array(
                'default'      => '',
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport'    => 'postMessage',
                'priority' => 15
                )
            );
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'top_info1', array(
                    'type' => 'textarea',
                    'label'    => 'Top Info 1',
                    'settings' => 'top_info1',
                    'section'  => 'top_info_options'
                    )
                )
            );
        // Text {2}
            $wp_customize->add_setting('top_info2', array(
                'default'      => '',
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport'    => 'postMessage',
                'priority' => 15
                )
            );
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'top_info2', array(
                    'type' => 'textarea',
                    'label'    => 'Top Info 2',
                    'settings' => 'top_info2',
                    'section'  => 'top_info_options'
                    )
                )
            );

        // Top Parallax
        $wp_customize->add_section('home_parallax_options', array(
            'title'       => __('Top Banner Image in Home page', 'DWA'),
            'description' => __('Upload a image to Top Parallax in home page', 'DWA'),
            'priority'    => 201,
            'panel'       => 'dwa_pages_options'
            )
        );
        $wp_customize->add_setting('home_parallax_image', array(
            'default'      => '',
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
            'transport'    => 'postMessage',
            'priority' => 15
            )
        );
        $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'home_parallax_image', array(
                'label'    => 'Background Image',
                'settings' => 'home_parallax_image',
                'section'  => 'home_parallax_options'
                )
            )
        );

        // Statistics section

        // {1}
        $wp_customize->add_section('statistics1_options', array(
            'title'       => __('Statistic 1', 'DWA'),
            'description' => __('Statistic 1', 'DWA'),
            'priority'    => 201,
            'panel'       => 'dwa_pages_options'
            )
        );
            // {1} Text
            $wp_customize->add_setting('statistics1_text', array(
                'capability' => 'edit_theme_options',
                'type' => 'theme_mod',
                'priority' => 15
                )
            );
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize,
                'statistics1_text', array(
                    'type' => 'text',
                    'label'    => 'Text',
                    'settings' => 'statistics1_text',
                    'section'  => 'statistics1_options'
                    )
                )
            );
            // {1} Numbers
            $wp_customize->add_setting('statistics1_number', array(
                'capability' => 'edit_theme_options',
                'type' => 'theme_mod',
                'priority' => 15
                )
            );
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize,
                'statistics1_number', array(
                    'type' => 'number',
                    'label'    => 'Number',
                    'settings' => 'statistics1_number',
                    'section'  => 'statistics1_options'
                    )
                )
            );

        // {2}
        $wp_customize->add_section('statistics2_options', array(
            'title'       => __('Statistic 2', 'DWA'),
            'description' => __('Statistic 2', 'DWA'),
            'priority'    => 201,
            'panel'       => 'dwa_pages_options'
            )
        );
            // {2} Text
            $wp_customize->add_setting('statistics2_text', array(
                'capability' => 'edit_theme_options',
                'type' => 'theme_mod',
                'priority' => 15
                )
            );
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize,
                'statistics2_text', array(
                    'type' => 'text',
                    'label'    => 'Text',
                    'settings' => 'statistics2_text',
                    'section'  => 'statistics2_options'
                    )
                )
            );
            // {2} Numbers
            $wp_customize->add_setting('statistics2_number', array(
                'capability' => 'edit_theme_options',
                'type' => 'theme_mod',
                'priority' => 15
                )
            );
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize,
                'statistics2_number', array(
                    'type' => 'number',
                    'label'    => 'Number',
                    'settings' => 'statistics2_number',
                    'section'  => 'statistics2_options'
                    )
                )
            );

        // {3}
        $wp_customize->add_section('statistics3_options', array(
            'title'       => __('Statistic 3', 'DWA'),
            'description' => __('Statistic 3', 'DWA'),
            'priority'    => 201,
            'panel'       => 'dwa_pages_options'
            )
        );
            // {3} Text
            $wp_customize->add_setting('statistics3_text', array(
                'capability' => 'edit_theme_options',
                'type' => 'theme_mod',
                'priority' => 15
                )
            );
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize,
                'statistics3_text', array(
                    'type' => 'text',
                    'label'    => 'Text',
                    'settings' => 'statistics3_text',
                    'section'  => 'statistics3_options'
                    )
                )
            );
            // {3} Numbers
            $wp_customize->add_setting('statistics3_number', array(
                'capability' => 'edit_theme_options',
                'type' => 'theme_mod',
                'priority' => 15
                )
            );
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize,
                'statistics3_number', array(
                    'type' => 'number',
                    'label'    => 'Number',
                    'settings' => 'statistics3_number',
                    'section'  => 'statistics3_options'
                    )
                )
            );

        // About Section
        $wp_customize->add_section('about_options', array(
            'title'       => __('About Section', 'DWA'),
            'description' => __('About Section', 'DWA'),
            'priority'    => 201,
            'panel'       => 'dwa_pages_options'
            )
        );
            // Image
            $wp_customize->add_setting('about_image', array(
                'default'      => '',
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport'    => 'postMessage',
                'priority' => 15
                )
            );
            $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'about_image', array(
                    'label'    => 'Background Image',
                    'settings' => 'about_image',
                    'section'  => 'about_options'
                    )
                )
            );
            // Title
            $wp_customize->add_setting('about_title', array(
                'default'      => '',
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport'    => 'postMessage',
                'priority' => 15
                )
            );
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'about_title', array(
                    'type' => 'text',
                    'label'    => 'Section Title',
                    'settings' => 'about_title',
                    'section'  => 'about_options'
                    )
                )
            );
            // Carousel Info
                // {1}
                $wp_customize->add_setting('about_info_slide1', array(
                    'default'      => '',
                    'type' => 'theme_mod',
                    'capability' => 'edit_theme_options',
                    'transport'    => 'postMessage',
                    'priority' => 15
                    )
                );
                $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'about_info_slide1', array(
                        'type' => 'textarea',
                        'label'    => 'Carousel Slide 1',
                        'settings' => 'about_info_slide1',
                        'section'  => 'about_options'
                        )
                    )
                );

                // {2}
                $wp_customize->add_setting('about_info_slide2', array(
                    'default'      => '',
                    'type' => 'theme_mod',
                    'capability' => 'edit_theme_options',
                    'transport'    => 'postMessage',
                    'priority' => 15
                    )
                );
                $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'about_info_slide2', array(
                        'type' => 'textarea',
                        'label'    => 'Carousel Slide 2',
                        'settings' => 'about_info_slide2',
                        'section'  => 'about_options'
                        )
                    )
                );
                // {3}
                $wp_customize->add_setting('about_info_slide3', array(
                    'default'      => '',
                    'type' => 'theme_mod',
                    'capability' => 'edit_theme_options',
                    'transport'    => 'postMessage',
                    'priority' => 15
                    )
                );
                $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'about_info_slide3', array(
                        'type' => 'textarea',
                        'label'    => 'Carousel Slide 3',
                        'settings' => 'about_info_slide3',
                        'section'  => 'about_options'
                        )
                    )
                );
                // {4}
                $wp_customize->add_setting('about_info_slide4', array(
                    'default'      => '',
                    'type' => 'theme_mod',
                    'capability' => 'edit_theme_options',
                    'transport'    => 'postMessage',
                    'priority' => 15
                    )
                );
                $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'about_info_slide4', array(
                        'type' => 'textarea',
                        'label'    => 'Carousel Slide 4',
                        'settings' => 'about_info_slide4',
                        'section'  => 'about_options'
                        )
                    )
                );

        // Directions section
        $wp_customize->add_section('directions_options', array(
            'title'       => __('Directions Section', 'DWA'),
            'description' => __('Directions Section', 'DWA'),
            'priority'    => 201,
            'panel'       => 'dwa_pages_options'
            )
        );
        // Tabs
            // Title {1}
            $wp_customize->add_setting('tab_title1', array(
                'default'      => '',
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport'    => 'postMessage',
                'priority' => 15
                )
            );
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'tab_title1', array(
                    'type' => 'text',
                    'label'    => 'Tab Title 1',
                    'settings' => 'tab_title1',
                    'section'  => 'directions_options'
                    )
                )
            );

            // Content {1}
            $wp_customize->add_setting('directions_tab1', array(
                'default'      => '',
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport'    => 'postMessage',
                'priority' => 15
                )
            );
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'directions_tab1', array(
                    'type' => 'textarea',
                    'label'    => 'Directions Tab 1',
                    'settings' => 'directions_tab1',
                    'section'  => 'directions_options'
                    )
                )
            );

            // Title {2}
            $wp_customize->add_setting('tab_title2', array(
                'default'      => '',
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport'    => 'postMessage',
                'priority' => 15
                )
            );
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'tab_title2', array(
                    'type' => 'text',
                    'label'    => 'Tab Title 2',
                    'settings' => 'tab_title2',
                    'section'  => 'directions_options'
                    )
                )
            );

            // Content {2}
            $wp_customize->add_setting('directions_tab2', array(
                'default'      => '',
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport'    => 'postMessage',
                'priority' => 15
                )
            );
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'directions_tab2', array(
                    'type' => 'textarea',
                    'label'    => 'Directions Tab 2',
                    'settings' => 'directions_tab2',
                    'section'  => 'directions_options'
                    )
                )
            );

        // Partners section
            // TODO : Multiple Image Upload
        $wp_customize->add_section( 'partners_section' , array(
            'title'       => __( 'Upload partner logo', 'DWA' ),
            'priority'    => 201,
            'description' => 'Upload partners logos',
            'panel'       => 'dwa_pages_options'
        ) );
            // Number of Images on Slide
        $wp_customize->add_setting('partners_count', array(
            'capability' => 'edit_theme_options',
            'type' => 'theme_mod',
            'priority' => 15
            )
        );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize,
            'partners_count', array(
                'type' => 'number',
                'label'    => 'Number',
                'settings' => 'partners_count',
                'section'  => 'partners_section'
                )
            )
        );

        // Image Upload Loop
        /*$pcount = get_theme_mod( 'partners_count' );

        foreach($pcount as $number){
            $wp_customize->add_setting('partner_image'.$number, array(
                'default'      => '',
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options'
                )
            );
            $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'partner_image'.$number, array(
                    'label'    => __( 'Partner Logo '.$number, 'DWA' ),
                    'section'  => 'partners_section',
                    'settings' => 'partner_image'.$number
                    )
                )
            );

        }*/

    // Blog page
        // Top Parallax
        $wp_customize->add_section('blog_parallax_options', array(
            'title'       => __('Top Banner Image in Home page', 'DWA'),
            'description' => __('Upload a image to Top Parallax in home page', 'DWA'),
            'priority'    => 201,
            'panel'       => 'dwa_blog_page_options'
            )
        );
        $wp_customize->add_setting('blog_parallax_image', array(
            'default'      => '',
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
            'transport'    => 'postMessage',
            'priority' => 15
            )
        );
        $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'blog_parallax_image', array(
                'label'    => 'Image',
                'settings' => 'blog_parallax_image',
                'section'  => 'blog_parallax_options'
                )
            )
        );
        // Text Title
            $wp_customize->add_setting('blog_title', array(
                'default'      => '',
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport'    => 'postMessage',
                'priority' => 15
                )
            );
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'blog_title', array(
                    'type' => 'text',
                    'label'    => 'Blog Title',
                    'settings' => 'blog_title',
                    'section'  => 'blog_parallax_options'
                    )
                )
            );
        // Text Description
            $wp_customize->add_setting('blog_descr', array(
                'default'      => '',
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport'    => 'postMessage',
                'priority' => 15
                )
            );
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'blog_descr', array(
                    'type' => 'text',
                    'label'    => 'Blog Description',
                    'settings' => 'blog_descr',
                    'section'  => 'blog_parallax_options'
                    )
                )
            );

}
add_action( 'customize_register', 'dwa_theme_customizer' );



/**
 * Pagination
 */
add_filter( 'redirect_canonical', 'custom_disable_redirect_canonical' );
function custom_disable_redirect_canonical( $redirect_url ) {
    if ( is_paged() && is_singular() ) $redirect_url = false;
    return $redirect_url;
}

 ?>