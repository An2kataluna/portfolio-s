<?php get_header(); ?>

<?php if ( is_front_page() && is_home() ) { ?>

    <!-- Default homepage -->

    <?php get_template_part( 'page' ); ?>

<?php } elseif ( is_front_page() ) { ?>

    <!-- static homepage -->

    <?php //get_template_part( 'home' ); ?>

<?php } elseif ( is_home() ) { ?>

    <!-- blog page -->

    <?php get_template_part( 'blog' ); ?>

<?php } else { ?>

    <!-- everything else -->

    <?php get_template_part( 'archive'); ?>

<?php } ?>


<?php get_footer(); ?>

