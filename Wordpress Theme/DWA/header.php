<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]>
<html class="no-js ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>
<html class="no-js ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html <?php language_attributes(); ?> > <!--<![endif]-->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>"> <!-- UTF-8 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1, shrink-to-fit=no">
    <meta name="format-detection" content="telephone=no">
    <title>Dynamic Web Applications</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
     <!--Import materialize.css-->
     <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/material-design-iconic-font.min.css" media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/materialize.min.css" media="screen,projection"/>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/custom.css">
    <?php // wp_head(); ?>

    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

    <script>
          function initMap() {

          }
    </script>
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAQ0Da1ZIZjqWrwNIctwaUH0_4AiwTjPbE&callback=initMap">
    </script>

    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>

    <header id="#top" name="top">
        <!-- top bar -->
        <nav class="primary-bg">
            <div class="nav-wrapper">
                <a href="#" data-activates="mobile-demo" class="button-collapse left">
                    <i class="material-icons left">menu</i>
                </a>
                <span class="left top-address">
                    <?php echo get_theme_mod( 'top_info1' ) ?>
                </span>
                <span class="right top-phone">
                    <?php echo get_theme_mod( 'top_info2' ) ?>
                </span>
                <a href="<?php echo get_home_url(); ?>" class="center brand-logo">
                    <?php

                        if (get_theme_mod( 'dwa_logo' )) {
                            echo "<img src='".esc_url( get_theme_mod( 'dwa_logo' ) )."'>";
                        } else {
                            echo get_theme_mod( 'logo_code' );
                        }

                    ?>
                </a>


            </div>

            <?php if ( has_nav_menu( 'primary' ) ) :
                wp_nav_menu( array(
                    'theme_location' => 'primary',
                    'menu_class'     => 'right hide-on-med-and-down top-menu'
                 ) );
            endif; ?>


            <?php if ( has_nav_menu( 'primary' ) ) :
                wp_nav_menu( array(
                    'theme_location' => 'primary',
                    'menu_id' => 'mobile-demo',
                    'menu_class' => 'side-nav'
                 ) );
            endif; ?>

        </nav>
    </header>